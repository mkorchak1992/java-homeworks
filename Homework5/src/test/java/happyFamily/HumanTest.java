package happyFamily;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
  Human childLora = new Human("Lora", "Child1", 2020);
  Human childLars = new Human("Lars", "child2", 2019);


  @Test
  void testToString() {
    assertEquals(getExpectedHuman(childLora),childLora.toString());
    assertEquals(getExpectedHuman(childLars),childLars.toString());

  }

  public String getExpectedHuman(Human expect) {
    return expect.getClass().getSimpleName() + "{ " +
            "name='" + expect.getName()+ '\'' + ", surname='" +
           expect.getSurname() + '\'' + ",year = " + expect.getYear() +
            ", iq = " + expect.getIq() + ",schedule = "
            + expect.getSchedule() + "}";
  }
}