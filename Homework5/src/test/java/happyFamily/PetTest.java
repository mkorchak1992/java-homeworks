package happyFamily;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class PetTest {

  Pet Tom = new Pet(Species.CAT, "Tom", 10, 70, "drink a lot", "sleep a lot");
  Pet Jerry = new Pet(Species.MOUSE, "Jerry", 10, 70, "run a lot", "eat a loe");

  @Test
  void testToString() {
    assertEquals(getExpectedPet(Tom), Tom.toString());
    assertEquals(getExpectedPet(Jerry), Jerry.toString());
  }

  public String getExpectedPet(Pet expect) {
    return expect.getSpecies() + "{" + "nickname = '" +
            expect.getNickname() + '\'' + ", age = " +
            expect.getAge() + ", trickLevel = " +
            expect.getTrickLevel() + ", habits = " +
            expect.getHabits() + "}";
  }
}