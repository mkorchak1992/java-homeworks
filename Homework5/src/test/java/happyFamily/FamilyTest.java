package happyFamily;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
  String[][] motherSchedule = {
          {DayOfWeek.MONDAY.name(), "Work at school"}};

  String[][] fatherSchedule = {
          {DayOfWeek.THURSDAY.name(), "Watch movie"}};

  Pet Rex = new Pet(Species.DOG, "Rex", 2, 60, "cut all peoples");
  Human motherSara = new Human("Sara", "Clark", 1992, 90, motherSchedule);
  Human fatherAlonso = new Human("Alonso", "Boston", 1990, 90, fatherSchedule);

  Human childLora = new Human("Lora", "Child1", 2020);
  Human childLars = new Human("Lars", "child2", 2019);
  Human SomeChild = new Human("SomeChild", "SomeChild", 1990);

  Family smallFamily = new Family(motherSara, fatherAlonso);


  @Test
  void testAddChild() {
    assertEquals(0, smallFamily.getChildren().length);
    smallFamily.addChild(SomeChild);
    assertEquals(1, smallFamily.getChildren().length);
    assertSame(smallFamily.getChildren()[0], SomeChild);
  }

  @Test
  void testToString() {
    assertEquals(getExpectedPetFamily(smallFamily), smallFamily.toString());
  }

  @Test
  void testDeleteChildByIndex() {
    smallFamily.addChild(childLora);
    smallFamily.addChild(childLars);

    assertSame(childLora, smallFamily.getChildren()[0]);
    assertSame(childLars, smallFamily.getChildren()[1]);

    smallFamily.deleteChild(0);
    assertSame(childLars, smallFamily.getChildren()[0]);

    int arrLength = smallFamily.getChildren().length;
    smallFamily.deleteChild(10);
    assertEquals(smallFamily.getChildren().length, arrLength);
  }

  @Test
  void testDeleteChildByName() {
    smallFamily.addChild(childLora);
    smallFamily.addChild(childLars);

    smallFamily.deleteChildByName(childLars);
    for (int i = 0; i < smallFamily.getChildren().length; i++) {
      assertNotSame(childLars, smallFamily.getChildren()[i]);
    }
    smallFamily.deleteChildByName(SomeChild);
    for (int i = 0; i < smallFamily.getChildren().length; i++) {
      assertNotSame(SomeChild, smallFamily.getChildren()[i]);
    }

  }

  @Test
  void testCountFamily() {
    assertEquals(2, smallFamily.countFamily());
    smallFamily.addChild(childLora);
    smallFamily.addChild(childLars);
    assertEquals(4, smallFamily.countFamily());
  }

  public String getExpectedPetFamily(Family expect) {
    return expect.getMother().toString() + "\n" +
            expect.getFather().toString() + "\n" + "children " +
            Arrays.toString(expect.getChildren()) + "\n" +
            (expect.getPet() == null ? "" : expect.getPet().toString());
  }

}