package happyFamily;

public class Main {

  public static void main(String[] args) {

    String[][] motherSchedule = {
            {DayOfWeek.MONDAY.name(), "Work at school"},
            {DayOfWeek.TUESDAY.name(), "Relax"},
            {DayOfWeek.WEDNESDAY.name(), "Watch movie"}};

    String[][] fatherSchedule = {
            {DayOfWeek.THURSDAY.name(), "Watch movie"},
            {DayOfWeek.FRIDAY.name(), "Go to club"},
            {DayOfWeek.SATURDAY.name(), "Go to Gym"},
            {DayOfWeek.SUNDAY.name(), "Play Poker"}
    };

    Pet Tom = new Pet(Species.CAT, "Tom", 10, 70, "drink a lot");
    Pet Jerry = new Pet(Species.MOUSE, "Jerry", 10, 70, "run a lot");
    Pet Rex = new Pet(Species.DOG, "Rex", 2, 60, "cut all peoples");

    Human motherSara = new Human("Sara", "Clark", 1992, 90, motherSchedule);
    Human fatherAlonso = new Human("Alonso", "Boston", 1990, 90, fatherSchedule);

    Human childLora = new Human("Lora", "Child1", 2020);
    Human childLars = new Human("Lars", "child2", 2019);


    Family firstFamily = new Family(motherSara, fatherAlonso);
    firstFamily.addChild(childLora);
    firstFamily.addChild(childLars);
    firstFamily.deleteChildByName(motherSara);
    firstFamily.setPet(Rex);
    System.out.println("Family\n" + firstFamily.toString());
    System.out.println("_____________________");

    childLora.feedPat(true);
    childLora.describePet();
    childLora.setIq(80);
    System.out.println(childLora.toString());
    System.out.println("_____________________");
    System.out.println(Rex.toString());


    int counter = 0;
    while (counter < 10000) {
      Human obj1 = new Human("R2D2", "R2D2", 1996);
      Human obj2 = new Human("Sitripio", "Sitripio", 1998);
      counter++;
    }


  }

}
