package happyFamily;

public enum Species {
  CAT,
  DOG,
  BIRD,
  HORSE,
  MOUSE,
  RABBIT,
  SNAKE,
  SPIDER
}
