package happyFamily;

import java.util.Arrays;

public class Family {

  private final Human mother;
  private final Human father;
  private Human[] children;
  private Pet pet;

  static {
    System.out.println("Family is initialised...");
  }

  {
    System.out.println("New Family was created");
  }


  public Family(Human mother, Human father) {
    this.mother = mother;
    this.father = father;
    this.children = new Human[0];
    this.mother.setFamily(this);
    this.father.setFamily(this);

  }

  protected void finalize() {
    System.out.println(this.toString() + " is deleting");
  }

  public int countFamily() {
    return children.length + 2;
  }

  public Human[] addChild(Human child) {
    child.setFamily(this);
    int newArrLength = children.length;
    children = Arrays.copyOf(children, children.length + 1);
    children[newArrLength] = child;
    return children;
  }

  public void deleteChildByName(Human child) {
    if (children.length != 0 && Arrays.asList(children).indexOf(child) != -1) {
      Human[] anotherArray = new Human[children.length - 1];
      for (int i = 0, k = 0; i < children.length; i++) {
        if (child.hashCode() == children[i].hashCode()) {
          if (child.equals(children[i])) continue;
        } else {
          anotherArray[k++] = children[i];
        }
      }
      children = anotherArray;
    }

  }

  public Human[] deleteChild(int index) {
    if (children == null || index < 0 || index > children.length) {
      return children;
    }
    Human[] anotherArray = new Human[children.length - 1];
    for (int i = 0, k = 0; i < children.length; i++) {
      if (i == index) {
        continue;
      }
      anotherArray[k++] = children[i];
    }
    return this.children = anotherArray;
  }

  public Human getMother() {
    return mother;
  }

  public Human getFather() {
    return father;
  }

  public Human[] getChildren() {
    return children;
  }

  public Pet getPet() {
    return pet;
  }

  public void setChildren(Human... children) {
    this.children = children;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (obj.getClass() != this.getClass()) return false;
    Family that = (Family) obj;
    return this.mother.equals(that.mother) && this.father.equals(that.father) && Arrays.equals(this.children, that.children);
  }

  @Override
  public int hashCode() {
    int res = mother.hashCode() + father.hashCode() * 31;
    return res;
  }

  @Override
  public String toString() {
    return this.mother.toString() + "\n" +
            this.father.toString() + "\n" + "children " +
            Arrays.toString(this.children) + "\n" +
            (this.pet == null ? "" : pet.toString());
  }


}
