package happyFamily;

import java.util.Arrays;

public class Pet {
  private Species species;
  private String nickname;
  private int age;
  private int trickLevel;
  private String[] habits;

  static {
    System.out.println("Pet initialised...");
  }
  {
    System.out.println("New Pet is creating");
  }

  public Pet(Species species, String nickname) {
    this.species = species;
    this.nickname = nickname;
  }

  public Pet(Species species, String nickname, int age, int trickLevel, String... habits) {
    this(species, nickname);
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
  }

  public Pet() {
  }

  protected void finalize() {
    System.out.println (nickname + " is deleting");
  }

  void eat() {
    System.out.println("Я кушаю!");
  }

  void respond() {
    System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.species);
  }

  void foul() {
    System.out.println("Нужно хорошо замести следы...\n");
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (obj.getClass() != this.getClass()) return false;
    Pet that = (Pet) obj;
    return this.nickname.equals(that.nickname) && this.species.equals(that.species) && this.age == that.age;
  }

  @Override
  public String toString() {
    return this.species + "{" + "nickname = '" +
            this.nickname + '\'' + ", age = " +
            this.age + ", trickLevel = " +
            this.trickLevel + ", habits = " +
            Arrays.toString(this.habits) + "}";
  }

  public Species getSpecies() {
    return this.species;
  }

  public String getNickname() {
    return this.nickname;
  }

  public int getAge() {
    return this.age;
  }

  public int getTrickLevel() {
    return this.trickLevel;
  }

  public String getHabits() {
    return Arrays.toString(this.habits);
  }

  public void setSpecies(Species species) {
    this.species = species;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public void setTrickLevel(int trickLevel) {
    if (trickLevel <= 0 || trickLevel > 100) {
      System.out.println("trickLevel must be from 0 to 100");
    } else {
      this.trickLevel = trickLevel;
    }
  }

  public void setHabits(String... habits) {
    this.habits = habits;
  }
}
