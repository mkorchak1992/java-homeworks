package hw3;

import java.util.Scanner;

public class taskPlanner {
  static int x = 7;
  static int y = 2;
  static String[][] scedule = new String[x][y];
  static String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
  static String[] tasks = {"Relax", "Go to Gym", "Read a book", "Go to school", "Write a letter", "Go to club", "Learn Java in DAN IT"};
  static Scanner scan = new Scanner(System.in);
  static boolean isExit = false;

  static void printTask(int day) {
    System.out.printf("Your tasks for %s: %s\n", scedule[day][0], scedule[day][1]);
  }

  static void feelArray(String[][] arr) {
    for (int i = 0; i < x; i++) {
      for (int j = 0; j < y; j++) {
        arr[i][0] = days[i];
        arr[i][j] = tasks[i];
      }
    }
  }

  static void switchDaysInfo() {

    System.out.println("\nPlease, input the day of the week: ");

    while (!isExit) {
      String userWord = scan.next().toLowerCase().trim();
      switch (userWord) {
        case "sunday":
          printTask(0);
          break;
        case ("monday"):
          printTask(1);
          break;
        case ("tuesday"):
          printTask(2);
          break;
        case ("wednesday"):
          printTask(3);
          break;
        case ("thursday"):
          printTask(4);
          break;
        case ("friday"):
          printTask(5);
          break;
        case ("saturday"):
          printTask(6);
          break;
        case ("exit"):
          isExit = true;
          break;
        default:
          System.out.println("Sorry, I don't understand you, please try again.");
      }
    }
  }


  public static void main(String[] args) {
    feelArray(scedule);
    switchDaysInfo();
  }
}
