package hw2;

import java.util.Arrays;
import java.util.Scanner;

public class AreaShooting {

  public static int randomRange(int min, int max) {
    int delta = max - min;
    return (int) (Math.random() * (delta + 1) + min);
  }

  public static void printField(char[][] area, int size) {

    for (int x = 0; x <= size; x++) {
      System.out.print(x + " | ");
    }
    System.out.println();

    for (int i = 0; i < size; i++) {
      System.out.print(i + 1 + " |");
      for (int j = 0; j < size; j++) {
        System.out.printf(" %c |",area[i][j]);
      }
      System.out.println();
    }
  }

  public static void main(String[] args) {

    System.out.println("\nAll set. Get ready to rumble!");
    int sizeArea = 5;
    char[][] arrOfChars = new char[sizeArea][sizeArea];
    Scanner scanner = new Scanner(System.in);
    int randomX = randomRange(0, 4);
    int randomY = randomRange(0, 4);

    boolean finish = false;

    for (int i = 0; i < arrOfChars.length; i++) {
      Arrays.fill(arrOfChars[i], '-');
    }

    do {
      printField(arrOfChars, sizeArea);
      int x = -1;
      int y = -1;

      while (!(x > 0 && x <= sizeArea)) {
        System.out.println("Введіть лінію для пострілу");
        while (!scanner.hasNextInt()) {
          System.out.println("Введeним повинно бути число від 1 до 5");
          scanner.next();
          System.out.println("Введіть лінію для пострілу");
        }
        x = scanner.nextInt();
      }
      x--;

      while (!(y > 0 && y <= sizeArea)) {
        System.out.println("Введіть стовбець для пострілу");
        while (!scanner.hasNextInt()) {
          System.out.println("Введeним повинно бути число від 1 до 5");
          scanner.next();
          System.out.println("Введіть стовбець для пострілу");
        }
        y = scanner.nextInt();
      }
      y--;

      if (x == randomX && y == randomY) {
        arrOfChars[x][y] = 'X';
        printField(arrOfChars, sizeArea);
        finish = true;

      } else {
        arrOfChars[x][y] = '*';
      }
    } while (!finish);
    System.out.println("You have won!");
  }

}
