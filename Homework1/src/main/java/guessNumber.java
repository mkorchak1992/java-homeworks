import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class guessNumber {
  static int[] allUsersNumbers = new int[0];

  public static void print(String s) {
    System.out.println(s);
  }

  public static int getRandomNumb(int maxNumber) {
    Random random = new Random();
    return random.nextInt(maxNumber + 1);
  }

  public static void checkUserNumb(int number1, int number2, String name) {
    if (number1 > number2) {
      print("Your number is too big. Please, try again.");
    } else if (number1 < number2) {
      print("Your number is too small. Please, try again.");
    } else {
      print("Congratulations," + name);
    }
  }


  public static void addNumbsToArr(int number) {
    int newArrLength = allUsersNumbers.length;
    allUsersNumbers = Arrays.copyOf(allUsersNumbers, allUsersNumbers.length + 1);
    allUsersNumbers[newArrLength] = number;
  }

  public static void main(String[] args) {

    print("Let the game begin!");
    int randomNum = getRandomNumb(100);
    Scanner in = new Scanner(System.in);

    String userName;
    do {
      print("Enter your name");
      userName = in.nextLine();
    } while (userName.length() == 0);

    print("Enter your number ");
    int userNumber;
    do {
      userNumber = in.nextInt();
      addNumbsToArr(userNumber);
      checkUserNumb(userNumber, randomNum, userName);
    } while (userNumber != randomNum);

// Way to save numbers with StringBuilder > then > convert to arr!
//    StringBuilder sb = new StringBuilder();
//    print("Enter your number ");
//    int userNumber;
//    do {
//      userNumber = in.nextInt();
//      sb.append(userNumber).append(",");
//      checkUserNumb(userNumber, randomNum, userName);
//    } while (userNumber != randomNum);
//    String[] destIndexArray = sb.toString().split(",");
//    Arrays.sort(destIndexArray);
//    System.out.printf("Your numbers was -> %s",Arrays.toString(destIndexArray));

    Arrays.sort(allUsersNumbers);
    System.out.printf("Your numbers was -> %s", Arrays.toString(allUsersNumbers));
  }
}
