package happyFamily;

import java.util.Arrays;
import java.util.Random;

public class Human {

  private String name;
  private String surname;
  private int year;
  private int iq;
  private Pet pet;
  private Family family;
  private String[][] schedule;

  public Human(String name, String surname, int year) {
    this.name = name;
    this.surname = surname;
    this.year = year;
  }

  public Human(String name, String surname, int year, Family family) {
    this(name, surname, year);
    this.family = family;
  }

  public Human(String name, String surname, int year, Family family, Human mother, Human father, int iq, Pet pet, String[][] schedule) {
    this(name, surname, year, family);
    this.iq = iq;
    this.pet = pet;
    this.schedule = schedule;
  }

  public Human() {
  }

  public boolean feedPat(boolean timeToFeed) {
    if(this.pet == null){
      System.out.println("You steel don't have Pet");
    }
    boolean isPetFull = false;
    if (timeToFeed) {
      System.out.printf("Хм... покормлю ка я %s\n", pet.getNickname());
      isPetFull = true;
    } else {
      Random random = new Random();
      int number = random.nextInt(101);
      if (number < pet.getTrickLevel()) {
        System.out.printf("Хм... покормлю ка я %s\n", pet.getNickname());
        isPetFull = true;
      } else {
        System.out.printf("Думаю, %s не голоден.\n", pet.getNickname());
      }
    }
    return isPetFull;
  }

  ;

  void greetPet() {
    System.out.printf("Привет %s\n", pet.getSpecies());
  }

  void describePet() {
    String trick = (pet.getTrickLevel() <= 50 ? "почти не хитрый" : "очень хитрый");
    System.out.printf("У меня есть %s, eмy %d лет, он %s\n", pet.getSpecies(), pet.getAge(), trick);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (obj.getClass() != this.getClass()) return false;
    Human that = (Human) obj;

    return this.year == that.year && this.name != null && this.name.equals(that.name) &&
            this.surname.equals(that.surname);
  }

  @Override
  public String toString() {
    return this.getClass().getName().substring(12) + "{ " +
            "name='" + this.name + '\'' + ", surname='" +
            this.surname + '\'' + ",year = " + this.year +
            ", iq = " + this.iq + ",schedule = "
            + Arrays.deepToString(this.schedule) + "}";
  }

  public String getName() {
    return this.name;
  }

  public String getSurname() {
    return this.surname;
  }

  public int getYear() {
    return this.year;
  }

  public int getIq() {
    return this.iq;
  }

  public Pet getPet() {
    return this.pet;
  }

  public Family getFamily() {
    return family;
  }

  public String getSchedule() {
    return Arrays.deepToString(this.schedule);
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public void setIq(int iq) {
    this.iq = iq;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  public void setSchedule(String[][] schedule) {
    this.schedule = schedule;
  }
}
