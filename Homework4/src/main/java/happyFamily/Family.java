package happyFamily;

import java.util.Arrays;

public class Family {

  private final Human mother;
  private final Human father;
  private Human[] children;
  private Pet pet;


  public Family(Human mother, Human father) {
    this.mother = mother;
    this.father = father;
    this.children = new Human[0];
  }

  public int countFamily() {
    return children.length + 2;
  }

  public Human[] addChild(Human child) {
    int newArrLength = children.length;
    children = Arrays.copyOf(children, children.length + 1);
    children[newArrLength] = child;
    child.setFamily(this);
    return children;
  }

  public Human[] deleteChild(int index) {
    if (children == null || index < 0 || index > children.length) {
      return children;
    }
    Human[] anotherArray = new Human[children.length - 1];
    for (int i = 0, k = 0; i < children.length; i++) {
      if (i == index) {
        continue;
      }
      anotherArray[k++] = children[i];
    }
    return this.children = anotherArray;
  }

  public Human getMother() {
    return this.mother;
  }

  public Human getFather() {
    return this.father;
  }

  public String getChildren() {
    return Arrays.toString(this.children);
  }

  public Pet getPet() {
    return this.pet;
  }

  public void setChildren(Human... children) {
    this.children = children;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (obj.getClass() != this.getClass()) return false;
    Family that = (Family) obj;
    return this.mother.equals(that.mother) && this.father.equals(that.father) && Arrays.equals(this.children,that.children);
  }

  @Override
  public String toString() {
    return this.mother.toString() + "\n" +
            this.father.toString() + "\n" + "children " +
            Arrays.toString(this.children) + "\n" +
            (this.pet == null ? "" : pet.toString());
  }


}
