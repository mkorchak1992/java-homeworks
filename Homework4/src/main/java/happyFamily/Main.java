package happyFamily;

public class Main {

  public static void main(String[] args) {

    String[][] schedule = {
            {"Monday", "Work hard"},
            {"Tuesday", "Go to Gym"},
            {"Wednesday", "Play Poker"},
            {"Thursday", "Watch movie"},
            {"Friday", "Go to club"},
            {"Saturday", "Relax"},
            {"Sunday", "Relax"}};

    Pet Tom = new Pet("Cat", "Tom", 10, 70, "drink a lot");
    Pet Jerry = new Pet("Mouse", "Jerry", 10, 70, "run a lot");
    Pet Rex = new Pet("Dog", "Rex", 2, 60, "cut all peoples");

    Human Sara = new Human("Sara", "Clark", 1992);
    Human Alonso = new Human("Alonso", "Boston", 1990);
    Sara.setSchedule(schedule);

    Human Lora = new Human("Lora", "Child1", 2020);
    Human Lars = new Human("Lars", "child2", 2019);


    Family firstFamily = new Family(Sara, Alonso);
    firstFamily.setChildren(Lora, Lars);
    firstFamily.setPet(Rex);

    System.out.println(firstFamily.toString());
    System.out.println("_____________________");


    Lora.setPet(Tom);
    Lora.greetPet();
    Lora.describePet();
    Lora.feedPat(false);
    Lora.setIq(80);
    System.out.println(Lora.toString());
    System.out.println("_____________________");
    System.out.println(Tom.toString());

  }

}
